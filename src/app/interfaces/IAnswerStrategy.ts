export interface IAnswerStrategy {
    checkAnswer(correctValue: any, inputValue: any): boolean
}