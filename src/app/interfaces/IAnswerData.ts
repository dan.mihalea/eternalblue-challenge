import { IAnswerStrategy } from "./IAnswerStrategy";

export interface IAnswerData {
    step: number,
    correctValue: any,
    wasSubmitted: boolean,
    provider: IAnswerStrategy
}