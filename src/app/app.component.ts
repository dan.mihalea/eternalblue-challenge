import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.addIcon('ro', './assets/flags/ro.svg');
    this.addIcon('en', './assets/flags/en.svg');
    this.addIcon('dk', './assets/flags/dk.svg');
  }

  addIcon(iconName: string, iconPath: string) {
    this.matIconRegistry.addSvgIcon(iconName, this.domSanitizer.bypassSecurityTrustResourceUrl(iconPath));
  }

  title = 'IFS.Aero';
}
