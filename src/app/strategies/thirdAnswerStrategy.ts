import { IAnswerStrategy } from "../interfaces/IAnswerStrategy";

export class ThirdAnswerStrategy implements IAnswerStrategy {
    checkAnswer(correctValue: any, inputValue: any): boolean {
        console.log(`From ${typeof(ThirdAnswerStrategy)}`)
        window.location.href = correctValue;
        return false;
    }
}