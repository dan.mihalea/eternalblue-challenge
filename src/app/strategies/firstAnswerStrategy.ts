import { IAnswerStrategy } from "../interfaces/IAnswerStrategy";

export class FirstAnswerStrategy implements IAnswerStrategy {

    checkAnswer(correctValue: any, inputValue: any): boolean {
        let decodedAnswer = String.fromCharCode(...correctValue);
        if (inputValue === decodedAnswer) {
            return true;
        }
        return false;
    }
}