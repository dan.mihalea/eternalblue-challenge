import { IAnswerStrategy } from "../interfaces/IAnswerStrategy";

export class SecondAnswerStrategy implements IAnswerStrategy {
    checkAnswer(correctValue: any, inputValue: any): boolean {        
        return correctValue as string == inputValue as string
    }
}