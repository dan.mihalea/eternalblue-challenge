
export class ArrayHelper {
    static arraysEqual(source: string[], compareTo: any[]): boolean {
        if (source.length !== compareTo.length) return false;

        for (let i = 0; i < source.length; i++) {            
            if (source[i].charCodeAt(0) !== compareTo[i]) return false;
        }

        return true;
    }

    static getByteArray(inputString: string): number[] {
        let byteArray = [];

        for (let charIndex = 0; charIndex < inputString.length; charIndex++) {
            let charCode = inputString.charCodeAt(0);
            byteArray.push(charCode);
        }
        return byteArray;
    }
}