import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AnswerService } from 'src/app/services/answer.service';
import { LanguageService } from 'src/app/services/language.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ifs-aero-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  allLanguages: any[] = []
  selectedLanguage: any;
  availableLanguages: any[] = [];

  riddle = new FormGroup({
    password: new FormControl('', [])
  })

  hideSolution = true;
  isCorrectAnswer = false;
  wasSubmitted = false;
  step = 1;
  imagePath = 'https://app.ifs.aero/eternalblue/BlueHint.jpg'
  maxSteps = 0;
  isComplete = false;

  constructor(
    private answerService: AnswerService,
    private translateService: TranslateService,
    private languageService: LanguageService
  ) {
  }
  ngOnInit(): void {
    this.maxSteps = this.answerService.answerData.length

    this.allLanguages = this.languageService.getAvailableLanguages();
    this.setDefaultLanguage();
  }

  setLanguage(language: any) {
    let [first] = this.allLanguages.filter(l => l.key == language)
    this.selectedLanguage = first;
    this.availableLanguages = this.allLanguages.filter(l=>l.key !== this.selectedLanguage.key)
    this.translateService.use(language);
  }

  setDefaultLanguage() {
    this.setLanguage(this.allLanguages
      .filter(l => l.key == environment.defaultLanguage)
      .map(l => l.key)
    );
  }

  get password() {
    return this.riddle.get('password')
  }

  checkAnswer(step: number) {
    this.wasSubmitted = true;
    this.isCorrectAnswer = this.answerService.checkAnswer(step, this.password?.value)
    this.answerService.setSubmittedStep(step, this.wasSubmitted)
    if (this.isCorrectAnswer) {
      this.step++
    }

    this.riddle.reset()
  }

  download() {
    this.answerService.checkAnswer(this.step, null)
  }

}
