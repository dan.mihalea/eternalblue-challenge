import { Injectable } from '@angular/core';
import { IAnswerData } from '../interfaces/IAnswerData';
import { FirstAnswerStrategy } from '../strategies/firstAnswerStrategy';
import { SecondAnswerStrategy } from '../strategies/secondAnswerStrategy';
import { ThirdAnswerStrategy } from '../strategies/thirdAnswerStrategy';

@Injectable({
  providedIn: 'root'
})
export class AnswerService {
  constructor() {
  }

  // mock data
  answerData: IAnswerData[] = [
    {
      step: 1,
      wasSubmitted: false,
      correctValue: [98, 108, 117, 101, 98, 49, 117, 101, 66, 108, 117, 101],
      provider: new FirstAnswerStrategy
    },
    {
      step: 2,
      correctValue: 'blue1sF0rev3r',
      wasSubmitted: false,
      provider: new SecondAnswerStrategy
    },
    {
      step: 3,
      wasSubmitted: false,
      correctValue: 'https://app.ifs.aero/eternalblue/Task.md',
      provider: new ThirdAnswerStrategy
    }
  ];

  setSubmittedStep(step: number, wasSubmitted:boolean) {
    this.answerData.map((item) => {
      if (item.step == step) item.wasSubmitted = wasSubmitted
    })
  }

  checkAnswer(step: number, inputValue: any): boolean {
    let stepStrategy = this.answerData.find(x => x.step === step)
    if (!stepStrategy) return false;
    if (!stepStrategy.provider) return false;

    let answerInstance = stepStrategy.provider;

    return answerInstance.checkAnswer(stepStrategy.correctValue, inputValue)
  }

}
