import { Injectable } from '@angular/core';
import languages from './../../assets/translations/_list.json'

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor() { }

  getAvailableLanguages() {
    return languages;
  }
}
